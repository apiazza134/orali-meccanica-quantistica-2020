![build status](https://gitlab.com/apiazza134/orali-meccanica-quantistica-2020/badges/master/pipeline.svg)

# Orali Meccanica Quantistica 2020

Il PDF più recente si può scaricare [qui](https://gitlab.com/apiazza134/orali-meccanica-quantistica-2020/-/jobs/artifacts/master/raw/MeccanicaQuantistica-orali.pdf?job=compile_pdf)